"use strict";
/*
Циклы используются чтобы не дублировать код многократно, когда есть необходимость повторять действия, вычисления до тех пор пока соблюдается определенное условие.
*/
let m = "";
let n = "";
while (!Number.isInteger(m) || !Number.isInteger(n) || m < 0 || n <= m) {
    m = +prompt("Please enter first positive integer (m) \n (a positive number without a fractional component):", "");
    n = +prompt("Please enter second positive integer (n)  \n it should be more than first integer (m):", "");
    if (!Number.isInteger(m) || !Number.isInteger(n) || m < 0 || n <= m) {
        alert("Error! Please be more attentive this time while entering numbers.");
    }
}
console.log(`Integers multiple of 5 in the range from 0 to ${m}:`);
let sum = 0;
for (let i = 0; i <= m; i++) {
    if(i % 5 === 0 && i > 0) {
        console.log(i);
        sum += i;
        }
    }
if (sum === 0) {
    console.log("Sorry, no numbers");
}
console.log(`Prime numbers in the range from ${m} to ${n}:`);
for (let a = m; a <= n; a++) {
    let flag = 0;
    for (let b = 2; b < a; b++) {
        if (a % b === 0) {
            flag = 1;
            break;
        }
    }
    if (flag === 0 && a > 1) {
        console.log(a);
    }
}